package com.poberezhnyk.enums;

public enum Stones {
    Alexandrite, BlueDiamond, Jadeite, Musgravite,
    PadparadshcaSapphire, RedBeryl, Taaffeite;
}
