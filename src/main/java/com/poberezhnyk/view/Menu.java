package com.poberezhnyk.view;

import com.poberezhnyk.controller.Manager;
import com.poberezhnyk.enums.Stones;
import com.poberezhnyk.model.Necklace;
import com.poberezhnyk.model.Stone;

import java.util.*;

public class Menu {
    static final int MENU_SIZE = 5;
    static final int NECKLACE_MENU_SIZE = 4;
    static final int NECKLACE_SUBMENU_SIZE = 4;
    public static Scanner scanner = new Scanner(System.in);
    private Manager manager;

    public Menu() {
        manager = new Manager();
        printMainMenu();
        actByMainMenuChoice();
    }

    private void actByMainMenuChoice() {
        int choice = choiceCheck(MENU_SIZE);
        while (choice != MENU_SIZE) {
            switch (choice) {
                case 1: {
                    if (manager.getStones().size() == 0) {
                        System.out.println("0 stones in storage");
                        break;
                    }
                    printStoneList(manager.getStones());
                }
                break;
                case 2: {
                    printEnumStones();
                    int stoneIndex = scanner.nextInt() - 1;
                    createNewStone(stoneIndex);
                }
                break;
                case 3: {
                    List<String> allTypes = printStoneTypes();
                    if (allTypes.size() == 0) {
                        System.out.println("nothing to remove");
                    }
                    int subMenuChoice = scanner.nextInt() - 1;
                    System.out.println("Choose stone to remove");
                    List<Stone> stonesByType = printStonesByType(allTypes.get(subMenuChoice));
                    subMenuChoice = scanner.nextInt() - 1;
                    manager.removeStone(stonesByType.get(subMenuChoice));
                }
                break;
                case 4: {
                    actByNecklaceMenuChoice();
                }
            }
            printMainMenu();
            choice = choiceCheck(MENU_SIZE);
        }
    }

    public void printMainMenu() {
        System.out.println("|********************MAIN MENU********************|");
        System.out.println("Choose action: ");
        System.out.println("1. Show all stones");
        System.out.println("2. Add new stone");
        System.out.println("3. Remove stone");
        System.out.println("4. Necklace menu");
        System.out.println("5. exit");
        System.out.println("|*************************************************|");
    }

    private void printNecklaceMenu() {
        System.out.println("|****************NECKLACE MENU****************|");
        System.out.println("Choose action: ");
        System.out.println("1. Create new necklace");
        System.out.println("2. Choose necklace from list");
        System.out.println("3. Remove necklace");
        System.out.println("4. Back to main menu");
        System.out.println("|*********************************************|");
    }

    private void printEnumStones() {
        System.out.println("Choose stone type: ");
        for (int i = 0; i < Stones.values().length; i++) {
            System.out.println((i + 1) + ". " + Stones.values()[i]);
        }
    }

    private List<String> printStoneTypes() {
        System.out.println("Choose stone type: ");
        List<String> stoneSet = new ArrayList<>(manager.getStoneTypes());
        for (int i = 0; i < stoneSet.size(); i++) {
            System.out.println((i + 1) + ". " + stoneSet.get(i));
        }
        return stoneSet;
    }

    private List<Stone> printStonesByType(String type) {
        System.out.println("Choose stone: ");
        List<Stone> stonesByType = manager.getStonesBy(type);
        for (int i = 0; i < stonesByType.size(); i++) {
            System.out.println((i + 1) + ". " + stonesByType.get(i).toString());
        }
        return stonesByType;
    }

    private List<Necklace> printNecklaces() {
        System.out.println("Choose necklace: ");
        List<Necklace> necklaceList = new ArrayList<>(manager.getNecklaces());
        for (int i = 0; i < necklaceList.size(); i++) {
            System.out.println((i + 1) + ". " + necklaceList.get(i).toString());
        }
        return necklaceList;
    }

    private boolean createNewStone(int type) {
        System.out.println("Enter weight: ");
        double weight = 0.0;
        try {
            weight = scanner.nextDouble();
            while (weight <= 0) {
                System.out.println("weight should be more than zero");
                weight = scanner.nextDouble();
            }
        } catch (InputMismatchException e) {
            scanner.reset();
            scanner.nextLine();
            System.out.println("Wrong weight. Stone not added.");
            return false;
        }
        return manager.addNewStone(weight, type);
    }

    private void actByNecklaceMenuChoice() {
        List<Necklace> necklaceList = manager.getNecklaces();
        Necklace necklace = null;
        printNecklaceMenu();
        int choice = choiceCheck(NECKLACE_MENU_SIZE);
        while (choice != NECKLACE_MENU_SIZE) {
            switch (choice) {
                case 1: {
                    System.out.println("Enter name for necklace");
                    scanner.nextLine();
                    String name = scanner.nextLine();
                    necklace = new Necklace(name);
                    actByNecklaceSubmenuChoice(necklace);
                    manager.addNecklace(necklace);
                }
                break;
                case 2: {
                    if (printNecklaces().size() != 0) {
                        int necklaceIndex = scanner.nextInt() - 1;
                        necklace = necklaceList.get(necklaceIndex);
                        actByNecklaceSubmenuChoice(necklace);
                    } else {
                        System.out.println("0 necklaces exist");
                    }
                }
                break;
                case 3: {
                    if (printNecklaces().size() != 0) {
                        int necklaceIndex = scanner.nextInt() - 1;
                        List<Stone> necklaceStones = necklace.getStoneList();
                        for (int i = 0; i < necklaceStones.size(); i++) {
                            manager.addStone(necklaceStones.get(i));
                        }
                        manager.removeNecklace(necklaceList.get(necklaceIndex));
                    } else {
                        System.out.println("0 necklaces exist");
                    }
                }
                break;
                case 4:
                    break;
            }
            printNecklaceMenu();
            choice = choiceCheck(NECKLACE_MENU_SIZE);
        }
    }

    private void printNecklaceSubmenu() {
        System.out.println("|****************NECKLACE SUBMENU****************|");
        System.out.println("Choose action: ");
        System.out.println("1. Add stone to necklace");
        System.out.println("2. Remove stone from necklace");
        System.out.println("3. Show total weight and price");
        System.out.println("4. Back");
        System.out.println("|************************************************|");
    }

    private void actByNecklaceSubmenuChoice(Necklace necklace) {
        List<Stone> stoneList = manager.getStones();
        printNecklaceSubmenu();
        int choice = choiceCheck(NECKLACE_SUBMENU_SIZE);
        while (choice != NECKLACE_SUBMENU_SIZE) {
            switch (choice) {
                case 1: {
                    if (stoneList.size() == 0) {
                        System.out.println("0 stones");
                        break;
                    }
                    System.out.println("Choose stone");
                    printStoneList(stoneList);
                    int stoneChoice = scanner.nextInt() - 1;
                    necklace.addToNecklace(stoneList.get(stoneChoice));
                    manager.removeStone(stoneList.get(stoneChoice));
                }
                break;
                case 2: {
                    List<Stone> necklaceStones = necklace.getStoneList();
                    if (necklaceStones.size() == 0) {
                        System.out.println("0 stones");
                        break;
                    }
                    System.out.println("Choose stone");
                    printStoneList(necklaceStones);
                    int stoneChoice = scanner.nextInt() - 1;
                    necklace.removeFromNecklace(necklaceStones.get(stoneChoice));
                    manager.addStone(necklaceStones.get(stoneChoice));
                }
                break;
                case 3: {
                    double totalWeight = 0.0;
                    double totalPrice = 0.0;
                    List<Stone> necklaceStones = necklace.getStoneList();
                    for (int i = 0; i < necklaceStones.size(); i++) {
                        totalWeight += necklaceStones.get(i).getWeight();
                        totalPrice += necklaceStones.get(i).getTotalPrice();
                    }
                    System.out.println("Necklace name ->" + necklace.getName()
                            + ", total: price = " + totalPrice
                            + ", weight = " + totalWeight);
                }
                break;
                case 4:
                    break;
            }
            printNecklaceSubmenu();
            choice = choiceCheck(NECKLACE_SUBMENU_SIZE);
        }
    }

    private int choiceCheck(int menuSize) {
        int choice;
        try {
            choice = scanner.nextInt();
            if (choice < 1 || choice > menuSize) {
                throw new InputMismatchException();
            }
        } catch (InputMismatchException e) {
            System.out.println("Wrong input. Try again");
            scanner.reset();
            scanner.nextLine();
            choice = 0;
        }
        return choice;
    }

    private void printStoneList(List<Stone> stoneList) {
        for (int i = 0; i < stoneList.size(); i++) {
            System.out.println((i + 1) + ". " + stoneList.get(i).toString());
        }
    }
}
