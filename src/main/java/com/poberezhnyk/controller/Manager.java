package com.poberezhnyk.controller;

import com.poberezhnyk.model.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Manager {
    private List<Stone> stones;
    private List<Necklace> necklaces;

    public Manager() {
        System.out.println("Welcome to stones manager.");
        stones = new ArrayList<Stone>();
        necklaces = new ArrayList<Necklace>();
    }

    public boolean addNewStone(double weight, int type) {
        return stones.add(createStone(weight, type));
    }

    public boolean addStone(Stone stone) {
        return stones.add(stone);
    }

    public boolean addNecklace(Necklace necklace) {
        return necklaces.add(necklace);
    }

    public boolean removeNecklace(Necklace necklace) {
        return necklaces.remove(necklace);
    }

    public boolean removeStone(Stone stone) {
        return stones.remove(stone);
    }

    public List<Stone> getStonesBy(String type) {
        List<Stone> stonesByType = new ArrayList<Stone>();
        for (Stone stone : stones) {
            if (stone.getClass().toString().equals(type)) {
                stonesByType.add(stone);
            }
        }
        return stonesByType;
    }

    public Set<String> getStoneTypes() {
        Set<String> types = new HashSet<String>();
        for (Stone stone : stones) {
            types.add(stone.getClass().toString()
                    .substring(stone.getClass().toString().lastIndexOf('.')+1));
        }
        return types;
    }

    private Stone createStone(double weight, int type) {
        switch (type) {
            case 0:
                return new Alexandrite(weight);
            case 1:
                return new BlueDiamond(weight);
            case 2:
                return new Jadeite(weight);
            case 3:
                return new Musgravite(weight);
            case 4:
                return new PadparadshcaSapphire(weight);
            case 5:
                return new RedBeryl(weight);
            case 6:
                return new Taaffeite(weight);
            default: {
                System.out.println("default");
                return new Stone(0, weight);
            }

        }
    }

    public List<Stone> getStones() {
        return stones;
    }

    public List<Necklace> getNecklaces() {
        return necklaces;
    }


}
