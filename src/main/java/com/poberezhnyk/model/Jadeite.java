package com.poberezhnyk.model;

public class Jadeite extends Stone {
    public Jadeite(double weight) {
        super(20_000, weight);
    }

    @Override
    public String toString() {
        return "Jadeite " + super.toString();
    }
}
