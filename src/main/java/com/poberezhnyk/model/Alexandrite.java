package com.poberezhnyk.model;

public class Alexandrite extends Stone {
    public Alexandrite(double weight) {
        super(12_000, weight);
    }

    @Override
    public String toString() {
        return "Alexandrite " + super.toString();
    }
}
