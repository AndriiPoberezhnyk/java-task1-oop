package com.poberezhnyk.model;

public class Stone {
    private int priceForCarat;
    private double totalPrice;
    private final double weight;

    public Stone(int priceForCarat, double weight) {
        this.priceForCarat = priceForCarat;
        this.weight = weight;
        calcTotalPrice();
    }

    private void calcTotalPrice() {
        totalPrice = priceForCarat * weight;
    }

    public int getPriceForCarat() {
        return priceForCarat;
    }

    public void setPriceForCarat(int priceForCarat) {
        this.priceForCarat = priceForCarat;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public double getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return ", priceForCarat=" + priceForCarat +
                ", totalPrice=" + totalPrice +
                ", weight=" + weight +
                ';';
    }
}
