package com.poberezhnyk.model;

public class PadparadshcaSapphire extends Stone {
    public PadparadshcaSapphire(double weight) {
        super(8_000, weight);
    }

    @Override
    public String toString() {
        return "PadparadshcaSapphire " + super.toString();
    }
}
