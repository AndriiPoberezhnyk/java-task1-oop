package com.poberezhnyk.model;

public class Taaffeite extends Stone {
    public Taaffeite(double weight) {
        super(2_500, weight);
    }

    @Override
    public String toString() {
        return "Taaffeite " + super.toString();
    }
}
