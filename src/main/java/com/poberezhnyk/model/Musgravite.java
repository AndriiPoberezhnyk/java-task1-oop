package com.poberezhnyk.model;

public class Musgravite extends Stone {
    public Musgravite(double weight) {
        super(35_000, weight);
    }

    @Override
    public String toString() {
        return "Musgravite " + super.toString();
    }
}
