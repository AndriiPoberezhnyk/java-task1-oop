package com.poberezhnyk.model;

public class RedBeryl extends Stone {
    public RedBeryl(double weight) {
        super(10_000, weight);
    }

    @Override
    public String toString() {
        return "RedBeryl " + super.toString();
    }
}


