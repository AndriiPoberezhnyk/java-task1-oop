package com.poberezhnyk.model;

import java.util.ArrayList;
import java.util.List;

public class Necklace {
    private final String name;
    private List<Stone> stoneList;

    public Necklace(String name) {
        this.name = name;
        this.stoneList = new ArrayList<Stone>();
    }

    public boolean addToNecklace(Stone stone) {
        return stoneList.add(stone);
    }

    public boolean removeFromNecklace(Stone stone) {
        return stoneList.remove(stone);
    }

    public String getName() {
        return name;
    }

    public List<Stone> getStoneList() {
        return stoneList;
    }

    @Override
    public String toString() {
        return "Necklace "
                + ", name='" + name + '\''
                + ", stoneList=" + stoneList
                + ';';
    }
}
