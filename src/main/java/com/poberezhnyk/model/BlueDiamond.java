package com.poberezhnyk.model;

public class BlueDiamond extends Stone {
    public BlueDiamond(double weight) {
        super(3_093_000, weight);
    }

    @Override
    public String toString() {
        return "BlueDiamond " + super.toString();
    }
}
